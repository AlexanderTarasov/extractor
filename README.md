    
# Zver/extractor

Extractor - Helps you extract exe, msi, 7z, zip, rar, cab
Encoder - Encode/decode files, strings, hardids

## Installing

add following to your **composer.json**

```
"repositories": [
    {
      "type": "git",
      "url": "https://AlexanderTarasov@bitbucket.org/AlexanderTarasov/composer.git"
    }
  ],
  "require": {
    "zver/extractor": "dev-master",
  },
```

